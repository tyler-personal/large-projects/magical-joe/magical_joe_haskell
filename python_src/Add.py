from enum import Enum

class Digit(Enum):
    Zero=1;One=2;Two=3;Three=4;Four=5;Five=6;Six=7;Seven=8;Eight=9;Nine=10

    succ = lambda s: s.value + 1
    pred = lambda s: s.value - 1

globals().update(Digit.__members__)

add = lambda xs: lambda ys = (
    __add_uncurry(xs, ys))
def __add_uncurry(xs, ys):
    if xs == [] and ys == []:
        return [Zero]
    elif ys == []:
        return xs
    elif xs == []:
        return ys
    else:
        if all(y == Zero for y in ys):
            return xs
        else:
            add(successor(xs))(predecessor(ys))

successor = move(One)(Zero)(Nine)(succ)
predecessor = move(Zero)(Nine)(Zero)(pred)

move = lambda emptyNum: lambda min: lambda max: lambda change: lambda digits: (
    __move_uncurry(emptyNum, min, max, change, digits))
def __move_uncurry(emptyNum, min, max, change, digits):
    
