def fib(n):
    result = [0, 1]
    for x in range(2, n + 1):
        result.append(result[-1] + result[-2])
    return result[n]
