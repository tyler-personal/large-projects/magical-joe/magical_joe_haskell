module AddTest where

import Control.Exception.Assert
import Add

claim b = print $ assert b "Success"

addTest = do
  testSuccessor
  testPredecessor
  testAddition

testSuccessor = do
  print $ "start testSuccessor"
  claim $ successor [Nine] == [One, Zero]
  claim $ successor [Nine, Nine] == [One, Zero, Zero] 
  claim $ successor [Nine, Four] == [Nine, Five] 
  claim $ successor [Five] == [Six] 
  claim $ successor [Nine, Five, Four, Five, Nine] == [Nine, Five, Four, Six, Zero]
  print $ "testSuccessor Success"


testPredecessor = do
  print $ "start testPredecessor"
  claim $ predecessor [Nine] == [Eight]
  claim $ predecessor [One, Zero, Zero] == [Nine, Nine]
  claim $ predecessor [Five] == [Four]
  claim $ predecessor [One, Four] == [One, Three]
  print $ "testPredecessor Success"

testAddition = do
  print $ "start testAddition"
  claim $ add [Nine] [One] == [One, Zero]
  claim $ add [Nine, Nine] [Two] == [One, Zero, One]
  claim $ add [Four] [Seven] == [One, One]
  claim $ add [One, Five, Zero] [One, Zero, One] == [Two, Five, One]
