module Add where

{-@ type NonEmpty a = {v:[a] | len v >= 0 } @-}

data Digit = 
  Zero | One | Two | Three | Four | Five | Six | Seven | Eight | Nine 
  deriving (Eq, Ord, Enum, Show)

(|>) x f = f x


{-@ add :: NonEmpty Digit -> NonEmpty Digit -> [Digit] @-}
add [] [] = [Zero]
add xs [] = xs
add [] ys = ys
add xs ys
  | all (== Zero) ys = xs
  | otherwise = add (successor xs) (predecessor ys)

successor = move One Zero Nine succ
predecessor = move Zero Nine Zero pred

move emptyNum min max change digits = dropWhile (== Zero) $ case start of
  [] -> [emptyNum] ++ end
  (x:xs) -> (reverse xs) ++ [change x] ++ end
  where
    takeAndDrop check xs = (takeWhile check xs, dropWhile check xs)
    (end, start) = reverse digits |> takeAndDrop (== max) |>
      \(xs, ys) -> (map (\_ -> min) xs, ys)

