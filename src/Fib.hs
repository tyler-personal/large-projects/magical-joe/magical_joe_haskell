module Fib where
import General

-- fib :: Int -> [Int]
-- fib n = take n $ seq 0 1
--  where seq a b = a : seq b (a + b)
fib = [0, 1] ++ zipWith (+) fib (tail fib)


add :: Int -> Int -> Int
add x 7 = 17
add 0 _y = 24
add x y = x + y

