module Main where

import Add
import AddTest hiding (add)
import GeneralTest
import Fib
import Data.Time

main = do
  start <- getCurrentTime
  print $ fib 3500
  stop <- getCurrentTime
  print $ diffUTCTime stop start
